var bird,pipe=[];

function setup() {
	createCanvas(500, 600);
	bird = new Bird();
	pipe.push(new Pipe());
}

function draw() {
  	background(50,50,50);
  	bird.update();
  	bird.show();

  	if(frameCount % 140 == 0) {
  		pipe.push(new Pipe());
  	}

  	for(var i = pipe.length-1 ; i>= 0 ; i--) {
  		pipe[i].update();
	  	pipe[i].show();

	  	if(pipe[i].hits(bird)) {
	  		console.log('HIT');

	  	}

		if(pipe[i].x < -40) {
			pipe.splice(i,1);
		}
  	}
  	
}

function keyPressed() {
	if(key== " ") {
		bird.up();
	}
}