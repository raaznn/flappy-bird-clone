function Bird() {

	this.y = height /2;
	this.x = 100;
	this.gravity = 0.8;
	this.lift = -14;
	this.velocity = 0;

	this.show = function() {
		fill(255,255,0);
		ellipse (this.x,this.y,30,30)
	}

	this.update = function() {
		this.velocity += (this.gravity);
		this.velocity *= 0.9;
		this.y += this.velocity

		if(this.y > height-15) {
			this.y = height-15; 
			this.velocity = 0;
		}
		if(this.y < 15) {
			this.y = 15; 
			this.velocity = 0;
		}
	}

	this.up = function () {
		this.velocity = this.lift;
		
	}



}