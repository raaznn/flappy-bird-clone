function Pipe() {

	this.top = random(height/3)+50;
	this.bottom = this.top + 150;
	this.x = width;
	this.w = 50;
	this.highlight = false;

	velocity= 1.5;
	this.show = function () {
		fill(204,250,200) ;
		if(this.highlight) {
			fill(250,50,50);
		}
		rect(this.x,0,this.w,this.top);
		rect(this.x,this.bottom,this.w,height);
	}

	this.update = function () {
		this.x -= velocity;
	}

	this.offscreen = function () {
	}

	this.hits = function (bird) {
		if(bird.x > this.x && bird.x < this.x+40) {
			if(bird.y < this.top || bird.y > this.bottom) {
				this.highlight = true;
				return true;	
			}
			
		}
		return false;
	}
}